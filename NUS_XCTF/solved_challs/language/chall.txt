Language-1:

We are totally going retro for the challenges in Misc. Stay tuned!

Hint: Languages are cool.

Solution: language is brainfuck, copy paste code into online interpreter

Flag: TheflagisXCTF{W3_L0v3_Es0t3ric_Pr0gr4mming_L4ngu4g3s}

Language-2:

Here is another one. :D

Hint: Maybe you should look at language 1 first.

Solution: The language is malboge.

Flag: As we have told you before, this CTF will have lots of different challenges so that everyone can have some fun. The flag is XCTF{O_Malbolge_Wh3r3_Ar7_Th0u}
