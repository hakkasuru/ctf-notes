Someone tried to send us a message by leaving this image on an imageboard. Lucky for us they didn't choose 4chan. Can you find the message?

Hint: How would you hide a flag in an image without too much trouble?

Solution: strings sanity-forensics.png | grep XCTF

Flag: Wow, you can't be stumped! The flag is XCTF{Its_2016_n0t_3x1f_scubbin}
