Story:

--------------------------------------------------------------------------------------------------------------------------------

An unknown enemy agent has been sent to capture one of our key leaders
We have intercepted communications from his handler Shannon. Intercepted Comms as below:

    Orders encrypted using Xor Perfect Secrecy system...

    13450f0c0a42071344541f0a4c1241131300544c00075404564c1e552f11061d1d490d06501c1a1d0100090b1600091152410b1e410d0b1e1552

    Decrypt it with the key I sent you before.
    Don't worry, this time, the key is as long as the message.
    Godspeed.

    PS: 154807490a410508070000001e0253411517454c1a050109044d1b160c001d1a17000815410f0a43444d04030b52011f17000e0640

Can you break the encryption? Find out who they are targetting!!

--------------------------------------------------------------------------------------------------------------------------------

Once you get the name of the target, submit it in the form of XCTF{name}

E.g. If the name is John, submit the flag as XCTF{John}

Hint: -NiL-

Flag:
