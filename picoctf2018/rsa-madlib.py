from pwn import *
import sys

def solve(a, b):
    if (a[0] == 'q' or a[0] == 'p'):
        if (b[0] == 'q' or b[0] == 'p'):
            c = int(a[1])*int(b[1])
            return c
        if(b[0] == 'n'):
            c = int(b[1])/int(a[1])
            return c

def totient(a, b):
    return (int(a[1])-1)*(int(b[1])-1)

def encrypt(m, e, n):
    x = int(m[1])**int(e[1])
    y = x % int(n[1])
    return y

def decrypt(c, d, n):
    x = pow(c,d,n)
    return x

def egcd(a, b):
    if (a == 0):
        return (b, 0, 1)
    g, y, x = egcd(b%a,a)
    return (g, x - (b//a) * y, y)

def modinv(a, m):
    g, x, y = egcd(a, m)
    if (g != 1):
        raise Exception('No modular inverse')
    return x%m

conn = remote('2018shell1.picoctf.com',40440)
#stage 1
print conn.recvuntil("#### NEW MADLIB ####\n")
a = ((conn.recvline().replace(' ','')).rstrip('\n')).split(':')
b = ((conn.recvline().replace(' ','')).rstrip('\n')).split(':')
print conn.recvuntil('IS THIS POSSIBLE and FEASIBLE? (Y/N):')
conn.sendline('y')
print conn.recvuntil(':')
conn.sendline(str(solve(a,b)))

#stage 2
print conn.recvuntil("#### NEW MADLIB ####\n")
a = ((conn.recvline().replace(' ','')).rstrip('\n')).split(':')
b = ((conn.recvline().replace(' ','')).rstrip('\n')).split(':')
print conn.recvuntil('IS THIS POSSIBLE and FEASIBLE? (Y/N):')
conn.sendline('y')
print conn.recvuntil(':')
conn.sendline(str(solve(a,b)))

#stage 3
print conn.recvuntil('IS THIS POSSIBLE and FEASIBLE? (Y/N):')
conn.sendline('n')

#stage 4
print conn.recvuntil("#### NEW MADLIB ####\n")
a = ((conn.recvline().replace(' ','')).rstrip('\n')).split(':')
b = ((conn.recvline().replace(' ','')).rstrip('\n')).split(':')
print conn.recvuntil('IS THIS POSSIBLE and FEASIBLE? (Y/N):')
conn.sendline('y')
print conn.recvuntil(':')
conn.sendline(str(totient(a,b)))

#stage 4
print conn.recvuntil("#### NEW MADLIB ####\n")
# print conn.recvline()
# print conn.recvline()
# print conn.recvline()
a = ((conn.recvline().replace(' ','')).rstrip('\n')).split(':')
b = ((conn.recvline().replace(' ','')).rstrip('\n')).split(':')
c = ((conn.recvline().replace(' ','')).rstrip('\n')).split(':')
print conn.recvuntil('IS THIS POSSIBLE and FEASIBLE? (Y/N):')
conn.sendline('y')
print conn.recvuntil(':')
# print str(encrypt(a,b,c))
conn.sendline(str(encrypt(a,b,c)))

#stage 5
print conn.recvuntil('IS THIS POSSIBLE and FEASIBLE? (Y/N):')
conn.sendline('n')

#stage 6
print conn.recvuntil("#### NEW MADLIB ####\n")
# print conn.recvline()
# print conn.recvline()
# print conn.recvline()
a = ((conn.recvline().replace(' ','')).rstrip('\n')).split(':')
b = ((conn.recvline().replace(' ','')).rstrip('\n')).split(':')
c = ((conn.recvline().replace(' ','')).rstrip('\n')).split(':')
print conn.recvuntil('IS THIS POSSIBLE and FEASIBLE? (Y/N):')
conn.sendline('y')
print conn.recvuntil(':')
t = totient(a,b)
d = modinv(int(c[1]),t)
conn.sendline(str(d))

#stage 7
print conn.recvuntil("#### NEW MADLIB ####\n")
# print conn.recvline()
# print conn.recvline()
# print conn.recvline()
a = ((conn.recvline().replace(' ','')).rstrip('\n')).split(':') #p
b = ((conn.recvline().replace(' ','')).rstrip('\n')).split(':') #ciphertext
c = ((conn.recvline().replace(' ','')).rstrip('\n')).split(':') #e
d = ((conn.recvline().replace(' ','')).rstrip('\n')).split(':') #n
print conn.recvuntil('IS THIS POSSIBLE and FEASIBLE? (Y/N):')
conn.sendline('y')
print conn.recvuntil(':')
# find q
q = ['q',str(int(d[1])/int(a[1]))]
# calculate d
t = totient(a,q)
dd = modinv(int(c[1]),t)
# decipher ciphertext
m = decrypt(int(b[1]), dd, int(d[1]))
conn.sendline(str(m))
conn.stream()

print m #plaintext decimal -> hex -> ascii
